const express = require('express');
const http = require('http');
const cors = require("cors");
const bodyParser = require('body-parser');
const app = express();
const morgan = require('morgan');
const port = process.env.PORT || 3000;
const server = http.createServer(app);
const koneksi = require('./koneksi');
const io = require("socket.io")(server,{
    cors: {
        origin:"*",
    }
});
var mySocket =  require('./socket/index');

//middleware
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(express.json());
app.use(cors());
// app.use('/socket', require('./middleware/router/socket'));
app.use('/controller', require('./middleware/router/users.js'));
app.use('/akun', require('./middleware/router/akun.js'));
app.use('/auth', require('./middleware/router/auth.js'));
app.use('/golongan', require('./middleware/router/golongan.js'));
app.use('/regu', require('./middleware/router/regu_kelompok.js'));
app.use('/jadwal', require('./middleware/router/jadwal.js'));
mySocket.conectSocket(server,io);

server.listen(port,"0.0.0.0",()=>{
    console.log("server started");
});
