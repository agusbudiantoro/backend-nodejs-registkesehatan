const get_regu = require('../../model/regu_kelompok/get');

exports.getRegu = async function(req,res){

    let resRegu = await get_regu.getRegu(res);
    res.status(200).json(resRegu);

}