const connection = require('../../../koneksi');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../../../config/secret');

const get_tabel_akun = require('../../model/akun/get');
const get_tabel_admin = require('../../model/admin/get');
const get_tabel_dokter = require('../../model/dokter/get');
const get_tabel_users = require('../../model/users/get');

exports.login= async function(req,res){
    let post = {
        "username":req.body.username,
        "password":req.body.password
    }

    let resGetAkun=[];
    let resGetPengguna=[];
    resGetAkun = await get_tabel_akun.getAkun(post,res);
    console.log(resGetAkun);
    if(resGetAkun.length != 0){
        bcrypt.compare(post.password, resGetAkun[0].password, async function (err, result) {
            if(result == true){
                var token = jwt.sign({ resGetAkun }, config.secret, {
                    expiresIn: 5760
                });

                if(resGetAkun[0].role == 1){
                    resGetPengguna= await get_tabel_admin.getAdmin(resGetAkun[0].id_pengguna,res);
                    let body ={
                        id_akun:resGetAkun[0].id_akun,
                        id_pengguna:resGetPengguna[0].id,
                        username:resGetAkun[0].username,
                        name:resGetPengguna[0].name,
                        current_message:resGetPengguna[0].current_message,
                        time:resGetPengguna[0].time,
                        icon:resGetPengguna[0].icon,
                        role:resGetAkun[0].role,
                        token:token
                    }
                    res.status(200).json(body);
                } else if(resGetAkun[0].role == 2){
                    resGetPengguna= await get_tabel_dokter.getDokter(resGetAkun[0].id_pengguna,res);
                    let body ={
                        id_akun:resGetAkun[0].id_akun,
                        id_pengguna:resGetPengguna[0].id,
                        username:resGetAkun[0].username,
                        name:resGetPengguna[0].name,
                        current_message:resGetPengguna[0].current_message,
                        time:resGetPengguna[0].time,
                        icon:resGetPengguna[0].icon,
                        role:resGetAkun[0].role,
                        token:token
                    }
                    res.status(200).json(body);
                } else if(resGetAkun[0].role == 3){
                    resGetPengguna= await get_tabel_users.getUsers(resGetAkun[0].id_pengguna,res);
                    let body ={
                        id_akun:resGetAkun[0].id_akun,
                        id_pengguna:resGetPengguna[0].id,
                        username:resGetAkun[0].username,
                        name:resGetPengguna[0].name,
                        current_message:resGetPengguna[0].current_message,
                        time:resGetPengguna[0].time,
                        icon:resGetPengguna[0].icon,
                        role:resGetAkun[0].role,
                        status_current_message:resGetPengguna[0].status_current_message,
                        tgl_lahir:resGetPengguna[0].tgl_lahir,
                        alamat:resGetPengguna[0].alamat,
                        jenis_kelamin:resGetPengguna[0].jenis_kelamin,
                        email:resGetPengguna[0].email,
                        token:token
                    }
                    res.status(200).json(body);
                }
            }else{
                res.status(403).send("password salah");
            }
        });
    }else{
        res.status(404).send("username salah");
    }

}

exports.cekToken = function (req, res) {
    var tokenWithBearer = req.headers.authorization;
        if(tokenWithBearer){
            var token = tokenWithBearer.split(' ')[1];
            //verif
            jwt.verify(token, config.secret, function(err, decode){
                if(err){
                    return res.status(401).send({auth:false,message:"token tiddak terdaftar atau expired", value:err});
                }else{
                    //cari token
                    res.status(200).send({message:"token tersedia"});
                }
            });
        }else{
            return res.status(401).send({auth:false,message:"token tiddak tersedia"});
        }
}