const get_golongan = require('../../model/golongan/get');

exports.getGolongan = async function(req,res){

    let resGol = await get_golongan.getGolongan(res);
    res.status(200).json(resGol);

}