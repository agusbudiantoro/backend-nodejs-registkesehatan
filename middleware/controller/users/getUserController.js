const tabel_users= require('../../model/users/getUsersModel')

exports.getUsers = async function(req,res){
    return tabel_users.getUsers(res);
}

exports.getUserById = async function(req,res){
    return tabel_users.getUserById(req.params.id,res);
}