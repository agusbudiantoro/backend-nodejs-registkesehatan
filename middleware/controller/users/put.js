const table_usersPut = require('../../model/users/put');

exports.dataDiriUser = async function(req,res){
    let body = {
        email:req.body.email,
        alamat:req.body.alamat,
        tgl_lahir:req.body.tgl_lahir,
        jenis_kelamin:req.body.jenis_kelamin,
    }

    return table_usersPut.editDiriUsers(body,req.params.id,res);
}

exports.uploadFile = async function(req,res){
    let body = {
        file:req.file.filename,
        status_regist:2
    }

    return table_usersPut.editDiriUsers(body,req.params.id,res);
}