const tabel_messages= require('../../model/messages/post')

exports.postMessages = async function(req){
    let body = {
        id_user_target:req.targetId,
        id_user:req.sourceId,
        pesan:req.message,
        status:1,
        waktu:req.waktu
    }
    return tabel_messages.postMessages(body);
}