const tabel_messages= require('../../model/messages/get')

exports.getMessages = async function(req,res){
    let body = {
        id_user_target:req.body.id_user_target,
        id_user:req.body.sourceid
    }
    console.log(body);
    let isi = await tabel_messages.getMessages(body.id_user_target,body.id_user,res);
    res.status(200).json(isi)
}