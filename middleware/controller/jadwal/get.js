const get_jadwal = require('../../model/jadwal/get');

exports.getJadwal = async function(req,res){

    let resJad = await get_jadwal.getJadwal(res);
    res.status(200).json(resJad);

}

exports.getJadwalByIdKelompok = async function(req,res){

    let resJad = await get_jadwal.getJadwalByIdKelompok(req.params.id,res)
    res.status(200).json(resJad);

}