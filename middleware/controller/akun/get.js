const tabel_user = require('../../model/users/getUsersModel');

exports.getAll = async function(req,res){
    let resdata = await tabel_user.getUsers(res);
    res.status(200).json(resdata);
}

exports.getUsersByStatus = async function(req,res){
    let resData = await tabel_user.getUsersByRoleByStatus(req.params.status,res);
    res.status(200).json(resData);
}