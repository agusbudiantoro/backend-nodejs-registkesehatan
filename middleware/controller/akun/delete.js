const tabel_akun = require("../../model/akun/delete");
const tabel_user = require("../../model/users/delete");

exports.deleteAkun = async function(req,res){
    let resAkun = await tabel_akun.hapusAkun(req.params.id,res);
    let resUser = await tabel_user.hapusUsers(req.params.id, res);
    return res.status(200).send("berhasil hapus akun");
}