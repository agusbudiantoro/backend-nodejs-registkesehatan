const postTabelAdmin = require('../../model/admin/post');
const postTabelDokter = require('../../model/dokter/post');
const postTabelUsers = require('../../model/users/post');
const getTabelAkun = require('../../model/akun/get');
const postTabelAkun = require('../../model/akun/post');
const bcrypt = require('bcrypt');
const saltRounds = 12;

exports.registrasiAkun = async function(req,res){
    bcrypt.hash(req.body.password, saltRounds,async function (err, hash) {
        let body = {
            username:req.body.username,
            name:req.body.name,
            current_message:"",
            time:"",
            icon:"person.svg",
            password:hash,
            role:req.body.role
        }
        let resPostPengguna = [];
        let resGetAkun =[];
        if(body.role == 1){// role 1 untuk admin
            resGetAkun = await getTabelAkun.getAkun(body,res);
            if(resGetAkun.length == 0){
                resPostPengguna = await postTabelAdmin.tambahAdmin(body,res);
                return postTabelAkun.tambahAkun(body,resPostPengguna.insertId,res);
            } else {
                res.status(404).send("akun sudah terdaftar");
            }
        }else if(body.role == 2){// role 2 untuk dokter
            resGetAkun = await getTabelAkun.getAkun(body,res);
            if(resGetAkun.length == 0){
                resPostPengguna = await postTabelDokter.tambahDokter(body,res);
                return postTabelAkun.tambahAkun(body,resPostPengguna.insertId,res);
            } else {
                res.status(404).send("akun sudah terdaftar");
            }
        }else if(body.role == 3){// role 3 untuk user
            resGetAkun = await getTabelAkun.getAkun(body,res);
            if(resGetAkun.length == 0){
                resPostPengguna = await postTabelUsers.tambahUser(body,res);
                return postTabelAkun.tambahAkun(body,resPostPengguna.insertId,res);
            } else {
                res.status(404).send("akun sudah terdaftar");
            }
        }
    });
}