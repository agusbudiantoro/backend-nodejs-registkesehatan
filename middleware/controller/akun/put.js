const getTabelAkun = require('../../model/akun/get');
const editTabelAkun = require('../../model/akun/put');
const bcrypt = require('bcrypt');
const saltRounds = 12;

exports.editAkun = async function(req,res){
    bcrypt.hash(req.body.password, saltRounds,async function (err, hash) {
        let body = {
            username:req.body.username,
            password:hash,
            id_akun:req.params.id_akun
        }
        let resGetAkun =[];
        resGetAkun = await getTabelAkun.getAkun(body,res);
            if(resGetAkun.length == 0){
                return editTabelAkun.editAkun(body,res);
            } else {
                res.status(404).send("akun sudah terdaftar");
            }
    });
}

exports.editGolongan = async function(req,res){
    console.log(req.body);
    let body = {
        golongan:req.body.golongan,
        regu_kelompok:req.body.regu_kelompok,
        status_regist:1
    }

    return editTabelAkun.editGolongan(body,req.params.id,res);
}

exports.resetGolongan = async function(req,res){
    let body = {
        golongan:0,
        regu_kelompok:0,
        status_regist:0
    }

    return editTabelAkun.editGolongan(body,req.params.id,res);
}

exports.verifStatusRegist = async function(req,res){
    let body = {
        status_regist:3
    }

    return editTabelAkun.editGolongan(body,req.params.id,res);
}