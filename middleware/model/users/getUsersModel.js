const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getUsers = async function (){
    return new Promise(function(resolve,reject){
        var query = "SELECT a.id, a.name, a.current_message, a.time, a.icon, a.status_current_message, b.id_akun, b.username FROM users AS a INNER JOIN tabel_akun AS b ON a.id=b.id_pengguna WHERE b.role=3"
        var table = [];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                reject(error)
            } else {
                resolve(rows)
            }
        });
    })
}

exports.getUsersByRoleByStatus = async function (status,res){
    return new Promise(function(resolve,reject){
        var query = "SELECT a.id, a.name, a.current_message, a.time, a.icon, a.status_current_message, a.file, a.status_regist,b.id_akun, b.username FROM users AS a INNER JOIN tabel_akun AS b ON a.id=b.id_pengguna WHERE b.role=3 ANd a.status_regist=?"
        var table = [status];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).json(error);
            } else {
                resolve(rows)
            }
        });
    })
}

exports.getUserById = async function (id,res){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM users WHERE id=?"
        var table = [id];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).json(error);
            } else {
                if(rows.length != 0){
                    res.status(200).json(rows[0]);
                }
            }
        });
    })
}