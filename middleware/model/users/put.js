const connection = require('../../../koneksi');
const mysql = require('mysql');


exports.editDiriUsers = async function(body,id_akun,res){

        var query = "UPDATE ?? SET ? WHERE id=?";
        var table = ["users",body,id_akun];
        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                res.status(200).send("berhasil edit users")
            }
        });
}

exports.editUsers = async function(body,res){
    let isi ={
            username:body.username,
            password:body.password,
    }
        var query = "UPDATE ?? SET ? WHERE id=?";
        var table = ["users",isi,body.id_akun];
        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                res.status(200).send("berhasil edit users")
            }
        });
}

exports.editCurrentMessageAndTime = async function(isi){
    return new Promise(function(resolve,reject){
        console.log(isi);
        let body ={
            id_akun:isi.sourceId,
            current_message:isi.message,
            time:isi.waktu
        }
            var query = "UPDATE ?? AS a INNER JOIN tabel_akun AS b ON a.id=b.id_pengguna SET a.current_message=?, time=? WHERE b.id_akun=?";
            var table = ["users",body.current_message,body.time,body.id_akun];
            query = mysql.format(query, table);
            connection.query(query, function (error, rows) {
                if (error) {
                    reject(error)
                } else {
                    resolve(rows)
                }
            });
    })
}