const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.tambahAkun = async function(body,idp,res){
    let isi ={
            username:body.username,
            id_pengguna:idp,
            password:body.password,
            role:body.role
    }
        var query = "INSERT INTO ?? SET ?";
        var table = ["tabel_akun"];
        query = mysql.format(query, table);
        connection.query(query, isi, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                res.status(200).send("berhasil menambahkan akun")
            }
        });
}