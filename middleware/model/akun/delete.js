const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.hapusAkun = async function(id_pengguna,res){
    return new Promise(function (resolve,reject){
        var query1 = "DELETE FROM ?? WHERE ??=?";
        var table1 = ["tabel_akun", "id_pengguna", id_pengguna];

                query1 = mysql.format(query1, table1);
                connection.query(query1, function (error, rows) {
                    if (error) {
                        res.status(501).json({pesan:"gagal", error:error});
                    } else {
                        resolve(rows);
                    }
                })
    });
}