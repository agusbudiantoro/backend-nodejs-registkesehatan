const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getMessages = async function(idt,myid,res){
    console.log(idt,myid);
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE id_user_target=? AND id_user=? OR id_user_target=? AND id_user=?";
        var table = ["messages",idt,myid,myid,idt];
        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                resolve(rows);
            }
        });
    });
}

exports.getMessagesSocket = async function(idt,myid){
    console.log(idt,myid);
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE id_user_target=? AND id_user=?";
        var table = ["messages",idt,myid];
        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}