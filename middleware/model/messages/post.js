const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.postMessages = async function (post){
    return new Promise(function(resolve,reject){
        var query = "INSERT INTO ?? SET ?";
        var table = ["messages"];
        query = mysql.format(query, table);
        connection.query(query, post, function (error, rows) {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}