const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getJadwal = async function(res){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ??";
        var table = ["jadwal"];
        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                resolve(rows);
            }
        });
    });
}

exports.getJadwalByIdKelompok = async function(idk,res){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE id_kelompok=?";
        var table = ["jadwal",idk];
        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                resolve(rows);
            }
        });
    });
}