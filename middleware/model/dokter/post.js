const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.tambahDokter = async function(body,res){
    let isi ={
        name:body.name,
        current_message:"",
        time:"",
        icon:"person.svg",
    }
    return new Promise(function(resolve,reject){
        var query = "INSERT INTO ?? SET ?";
        var table = ["dokter"];
        query = mysql.format(query, table);
        connection.query(query, isi, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                resolve(rows);
            }
        });
    })
}