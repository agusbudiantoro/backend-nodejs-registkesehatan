const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getGolongan = async function(res){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ??";
        var table = ["golongan"];
        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                resolve(rows);
            }
        });
    });
}