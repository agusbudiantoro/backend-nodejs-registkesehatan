const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getAdmin = async function(idp,res){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE id=?";
        var table = ["admin",idp];
        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                resolve(rows);
            }
        });
    });
}