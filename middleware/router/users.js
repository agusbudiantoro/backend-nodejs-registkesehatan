const express = require('express');
const router = express.Router();
const multer = require("multer");
const path = require("path");

const storage = multer.diskStorage({
    destination: './upload/file',
    filename: (req, file, cb) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`);
    }
});

var upload = multer({
    storage: storage,
    limits: { fileSize: 10000000 }
})

router.use(express.static('public'));

const tabel_user = require('../controller/users/getUserController');
const edit_tabel_user = require('../controller/users/put');
const download = require('../controller/downloadFile/download');

router.get('/api/v1/getUsers', tabel_user.getUsers);
router.get('/api/v1/getUserById/:id', tabel_user.getUserById);
router.put('/api/v1/editDataDiri/:id', edit_tabel_user.dataDiriUser);
router.put('/api/v1/uploadFile/:id',upload.single("file"), edit_tabel_user.uploadFile);

//download file
router.get('/api/v1/download/:namefile', download.downloadFile);

module.exports = router;