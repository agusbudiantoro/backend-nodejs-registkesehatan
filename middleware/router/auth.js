const express = require('express');
const router = express.Router();

const controller_auth = require('../controller/auth/login');

router.post('/api/v1/login', controller_auth.login);
router.post('/api/v1/cekToken', controller_auth.cekToken);

module.exports = router;