const express = require('express');
const router = express.Router();

const get_regu = require('../controller/regu_kelompok/get');

router.get('/api/v1/getReguKelompok', get_regu.getRegu);

module.exports = router;