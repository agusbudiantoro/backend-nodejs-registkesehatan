const express = require('express');
const router = express.Router();

const get_jadwal = require('../controller/jadwal/get');

router.get('/api/v1/getAllJadwal', get_jadwal.getJadwal);
router.get('/api/v1/getJadwalByIdKelompok/:id', get_jadwal.getJadwalByIdKelompok);

module.exports = router;