const express = require('express');
const router = express.Router();

const get_golongan = require('../controller/golongan/get');

router.get('/api/v1/getGolongan', get_golongan.getGolongan);

module.exports = router;