const express = require('express');
const router = express.Router();

const post_controller_akun = require('../controller/akun/post');
const put_controller_akun = require('../controller/akun/put');
const get_controller_akun = require('../controller/akun/get');
const delete_controller_akun = require('../controller/akun/delete');

const getMessages = require('../controller/messages/get');

router.post('/api/v1/createAkun', post_controller_akun.registrasiAkun);
router.put('/api/v1/editAkun/:id_akun', put_controller_akun.editAkun);
router.put('/api/v1/editGolongan/:id', put_controller_akun.editGolongan);
router.put('/api/v1/resetGolongan/:id', put_controller_akun.resetGolongan);
router.put('/api/v1/verifStatusRegist/:id', put_controller_akun.verifStatusRegist);
router.get('/api/v1/getAllAkun', get_controller_akun.getAll);
router.get('/api/v1/getUserByStatus/:status', get_controller_akun.getUsersByStatus);
router.delete('/api/v1/deleteAkun/:id', delete_controller_akun.deleteAkun);

router.get('/api/v1/getMessages', getMessages.getMessages);

module.exports = router;