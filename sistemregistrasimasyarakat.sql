-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 15, 2022 at 04:01 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistemregistrasimasyarakat`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `current_message` text NOT NULL,
  `time` varchar(20) NOT NULL,
  `icon` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `current_message`, `time`, `icon`) VALUES
(9, 'admin', '', '', 'person.svg');

-- --------------------------------------------------------

--
-- Table structure for table `dokter`
--

CREATE TABLE `dokter` (
  `id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `current_message` text NOT NULL,
  `time` varchar(20) NOT NULL,
  `icon` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokter`
--

INSERT INTO `dokter` (`id`, `name`, `current_message`, `time`, `icon`) VALUES
(1, 'dokter', '', '', 'person.svg');

-- --------------------------------------------------------

--
-- Table structure for table `golongan`
--

CREATE TABLE `golongan` (
  `id_golongan` int(20) NOT NULL,
  `nama_golongan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `golongan`
--

INSERT INTO `golongan` (`id_golongan`, `nama_golongan`) VALUES
(1, 'DM'),
(2, 'HIPERTENSI');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(20) NOT NULL,
  `id_kelompok` int(20) NOT NULL,
  `id_golongan` int(11) NOT NULL,
  `hari` varchar(50) NOT NULL,
  `tgl` varchar(50) NOT NULL,
  `jam` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `id_kelompok`, `id_golongan`, `hari`, `tgl`, `jam`) VALUES
(1, 1, 1, 'Jumat', '11/02/2022', '07.00'),
(2, 1, 1, 'Jumat', '11/03/2022', '07.00'),
(3, 1, 1, 'Jumat', '08/04/2022', '07.00'),
(4, 2, 2, 'Jumat', '18/02/2022', '07.00'),
(5, 2, 2, 'Jumat', '18/03/2022', '07.00'),
(6, 2, 2, 'Jumat', '15/04/2022', '07.00');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(20) NOT NULL,
  `id_user_target` int(20) NOT NULL,
  `id_user` int(20) NOT NULL,
  `pesan` text NOT NULL,
  `waktu` varchar(250) NOT NULL,
  `status` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `id_user_target`, `id_user`, `pesan`, `waktu`, `status`) VALUES
(80, 2, 4, 'Tes', ' 21:26', 1),
(81, 2, 4, 'Ha', ' 21:26', 1),
(82, 4, 2, 'ya', ' 21:26', 1),
(83, 2, 4, 'Tes', ' 21:27', 1),
(84, 2, 4, 'Yrs', ' 21:27', 1),
(85, 2, 3, 'tes', ' 21:28', 1),
(86, 3, 2, 'Hk', ' 21:28', 1),
(87, 2, 3, 'hakk', ' 21:29', 1),
(88, 3, 2, 'Tes', ' 21:29', 1),
(89, 2, 4, 'user2', ' 21:30', 1),
(90, 4, 2, 'Yes', ' 21:30', 1),
(91, 4, 2, 'Yes', ' 21:31', 1),
(92, 2, 4, 'tes', ' 21:31', 1),
(93, 4, 2, 'Y', ' 21:31', 1),
(94, 2, 3, 'hallo', ' 21:32', 1),
(95, 3, 2, 'Yes', ' 21:32', 1),
(96, 2, 3, 'yesss', ' 21:32', 1),
(97, 2, 3, 'hallo', ' 21:33', 1),
(98, 2, 3, 'tes', ' 21:34', 1),
(99, 3, 2, 'Iyaa', ' 21:34', 1),
(100, 2, 4, 'tes', ' 21:34', 1),
(101, 4, 2, 'Ok', ' 21:34', 1),
(102, 4, 2, 'Okk', ' 21:35', 1),
(103, 3, 2, 'Tes', ' 21:40', 1),
(104, 2, 4, 'cek', ' 21:44', 1),
(105, 4, 2, 'Yes', ' 21:44', 1),
(106, 2, 4, 'tes', ' 21:45', 1),
(107, 2, 4, 'Hayy', ' 21:46', 1),
(108, 4, 2, 'hallo', ' 21:46', 1),
(109, 2, 4, 'Hay', ' 21:47', 1),
(110, 2, 3, 'Oke', ' 21:47', 1),
(111, 3, 2, 'es', ' 21:47', 1),
(112, 2, 5, 'Halli', ' 09:11', 1),
(113, 4, 2, 'Tes', ' 21:58', 1),
(114, 3, 2, 'Cek', ' 21:58', 1),
(115, 2, 3, 'Oke', ' 21:58', 1);

-- --------------------------------------------------------

--
-- Table structure for table `regu_kelompok`
--

CREATE TABLE `regu_kelompok` (
  `id_kelompok` int(20) NOT NULL,
  `nama_kelompok` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `regu_kelompok`
--

INSERT INTO `regu_kelompok` (`id_kelompok`, `nama_kelompok`) VALUES
(1, 'Dahlia (DM)'),
(2, 'Reflesia (HIPERTENSI)');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_akun`
--

CREATE TABLE `tabel_akun` (
  `id_akun` int(20) NOT NULL,
  `id_pengguna` int(20) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_akun`
--

INSERT INTO `tabel_akun` (`id_akun`, `id_pengguna`, `username`, `password`, `role`) VALUES
(1, 9, 'admin', '$2b$12$DVQhnWQ.5Vr91ZK3WmiLzucQ7ClvQqNZ.nL2vmm9tGqGlvZIHsWrK', 1),
(2, 1, 'dokter', '$2b$12$KWFovMLSVLfvVgNElYtPausaVAJiIjesZZdXrImzGGKrbe5oACXai', 2),
(3, 3, 'user1', '$2b$12$8nbus0Qw0lGDMw5MJ5Fw3ec0jUfdsd/l1PpAitRO1sET0cF8eTgGi', 3),
(4, 4, 'user2', '$2b$12$KJoueYgqjOCRwj/NZIt9Z.dXo.rP7HZHZsI3dFi2FEQZQdSNp9Bu2', 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `current_message` text NOT NULL,
  `time` varchar(20) NOT NULL,
  `icon` varchar(20) NOT NULL,
  `status_current_message` int(11) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `email` varchar(150) NOT NULL,
  `golongan` int(20) NOT NULL,
  `regu_kelompok` int(20) NOT NULL,
  `status_regist` int(20) NOT NULL,
  `file` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `current_message`, `time`, `icon`, `status_current_message`, `tgl_lahir`, `alamat`, `jenis_kelamin`, `email`, `golongan`, `regu_kelompok`, `status_regist`, `file`) VALUES
(2, 'kuya', 'hey', '15:55', 'person.svg', 1, '0000-00-00', '', 'L', '', 0, 0, 0, ''),
(3, 'user1', 'Oke', ' 21:58', 'person.svg', 0, '2022-03-01', 'Ciracas ', 'L', 'User1@gmail.com', 0, 0, 0, 'file_1644935699703.pdf'),
(4, 'user2', 'Hay', ' 21:47', 'person.svg', 0, '0000-00-00', '', 'L', '', 0, 0, 0, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `golongan`
--
ALTER TABLE `golongan`
  ADD PRIMARY KEY (`id_golongan`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regu_kelompok`
--
ALTER TABLE `regu_kelompok`
  ADD PRIMARY KEY (`id_kelompok`);

--
-- Indexes for table `tabel_akun`
--
ALTER TABLE `tabel_akun`
  ADD PRIMARY KEY (`id_akun`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `dokter`
--
ALTER TABLE `dokter`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `golongan`
--
ALTER TABLE `golongan`
  MODIFY `id_golongan` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;
--
-- AUTO_INCREMENT for table `regu_kelompok`
--
ALTER TABLE `regu_kelompok`
  MODIFY `id_kelompok` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tabel_akun`
--
ALTER TABLE `tabel_akun`
  MODIFY `id_akun` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
