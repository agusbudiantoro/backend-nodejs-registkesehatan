// const express = require('express');
// const path = require("path");
// const app = express();
// const router = express.Router();
// const http = require('http');
// const server = http.createServer(app);
// const io = require("socket.io")(server,{
//     cors: {
//         origin:"*",
//     }
// });

const tabel_post_messages = require('../middleware/controller/messages/post');
const tabel_gett_messages = require('../middleware/model/messages/get');
const tabel_get_users=require('../middleware/model/users/getUsersModel');
const tabel_put_users=require('../middleware/model/users/put');

let client ={};

exports.conectSocket = async function(server,io){
   io.on("connection", (socket)=>{
        console.log("connected"); 
        console.log(socket.id, "has joined");
        socket.on("signin", (id)=>{
            console.log(id);
            client[id]=socket;
        });
        socket.on("message",(msg)=>{
            console.log(msg);
            let targetId = msg.targetId;
            tabel_post_messages.postMessages(msg);
            if(msg.sourceId != 2){
                tabel_put_users.editCurrentMessageAndTime(msg);
            }
            if(client[targetId]) client[targetId].emit("message", msg);
            
        });
        socket.on("getchat",async(id)=>{
            console.log(id);
            console.log("cekkk");
            let isi = await tabel_gett_messages.getMessages(id.targetId, id.sourceId);
            socket.emit('getchat', isi);
        });
        socket.on("getusers",async()=>{
            console.log("listuser");
            let isi = await tabel_get_users.getUsers();
            // console.log(isi);
            socket.emit("getusers",isi);
        });
    })
    io.on("disconnect", (socket)=>{
        console.log("disconnect");
    })
}
// router.use('/chat', (req,res)=>{
    
// });